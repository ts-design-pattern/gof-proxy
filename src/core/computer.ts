/**
 * 專案名稱： gof-proxy
 * 檔案說明： 抽象電腦
 * -----------------------------------------------------------------------------
 * @NOTE
 */
/* eslint-disable no-undef */

/**
 * 抽象電腦
 */
export abstract class Computer {
  /**
   * 電腦溫度
   */
  public temperature = Math.random() * 50 + 50;

  /**
   * 啟動電腦
   *
   * @method public
   */
  public abstract launch(): void;
}
