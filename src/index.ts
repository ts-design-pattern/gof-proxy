/**
 * 專案名稱： gof-proxy
 * 檔案說明： 代理者模式範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Pc, PcMonitorProxy } from './shared';

const pc = new Pc();
new PcMonitorProxy(pc).launch();
