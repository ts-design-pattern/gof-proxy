/**
 * 專案名稱： gof-proxy
 * 檔案說明： 電腦代理者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Computer } from './../core';

/**
 * 電腦代理者
 */
export class PcMonitorProxy extends Computer {
  /**
   * @param _computer 電腦
   */
  constructor(private _computer: Computer) {
    super();
  }

  /**
   * 溫度是否正常
   *
   * @method private
   * @param temperature 溫度
   * @return 回傳溫度是否正常
   */
  private isTemperatureNormal(temperature: number): boolean {
    return temperature < 70;
  }

  /**
   * 啟動電腦
   *
   * @method public
   */
  public launch(): void {
    this._computer.launch();
    setInterval(() => {
      const isNormal = this.isTemperatureNormal(this._computer.temperature);
      console.log(
        `Computer temperature: ${this._computer.temperature}°C over 70°C is ${
          isNormal ? 'normal' : 'abnormal'
        }`,
      );
    }, 1000);
  }
}
