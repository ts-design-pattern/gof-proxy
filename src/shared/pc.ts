/**
 * 專案名稱： gof-proxy
 * 檔案說明： 電腦
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Computer } from './../core';

/**
 * 電腦
 */
export class Pc extends Computer {
  constructor() {
    super();
    setInterval(() => (this.temperature = Math.random() * 50 + 50), 1000);
  }

  /**
   * 啟動電腦
   *
   * @method public
   */
  public launch(): void {
    console.log('PC launch');
  }
}
