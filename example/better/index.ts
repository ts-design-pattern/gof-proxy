/**
 * 專案名稱： gof-proxy
 * 檔案說明： 較佳的開發模式程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { AlarmRetryProxy, AlarmService } from './shared';

const alarmService = new AlarmService();
const alarmProxy = new AlarmRetryProxy(alarmService);
alarmProxy.send({
  eventId: 'SYS001',
  syncId: 'WKS.F232.CA1',
  eventTime: 1609459200000,
  message: '停線超過 2 小時',
  alarmType: 1,
  level: 'L4',
});
