/**
 * 專案名稱： gof-proxy
 * 檔案說明： 報警重新拋送代理者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as fs from 'fs';
import { Subject } from 'rxjs';
import { delay, filter, tap } from 'rxjs/operators';
import { Produce } from '../../core';
import { Alarm } from '../models';

/**
 * 重拋資料格式
 */
type ErrorRetry = { count: number; alarm: Alarm };

/**
 * 報警重新拋送代理者
 */
export class AlarmRetryProxy implements Produce {
  /**
   * 重新拋送機制
   */
  private _retry = new Subject<ErrorRetry>();

  /**
   * @param _alarmService 報警服務
   */
  constructor(private _alarmService: Produce) {
    this._retry
      .pipe(
        // 延遲 1 秒再送一次
        delay(1000),
        // 撰寫失敗日誌
        tap(res => this.writeLog(res)),
        // 次數上限 3 次
        filter(res => res.count <= 3),
      )
      .subscribe(res => this.retry(res));
  }

  /**
   * 撰寫失敗日誌
   *
   * @method private
   * @param response 補送結果
   */
  private writeLog(response: ErrorRetry): void {
    if (response.count > 3) {
      const filename = `./logs/${new Date().getTime()}.json`;
      if (!fs.existsSync('./logs')) {
        fs.mkdirSync('./logs');
      }
      fs.writeFileSync(filename, JSON.stringify(response.alarm));
    }
  }

  /**
   * 重新拋送
   *
   * @method private
   * @param response 補送結果
   */
  private retry(response: ErrorRetry): void {
    console.log(`Retry ${response.count + 1} times`);
    try {
      this._alarmService.send(response.alarm);
    } catch (error: any) {
      console.error(error.message);
      response.count++;
      this._retry.next(response);
    }
  }

  /**
   * 發送報警
   *
   * @method public
   * @param alarm 報警資料
   */
  public send(alarm: Alarm): void {
    try {
      this._alarmService.send(alarm);
    } catch (error: any) {
      console.error(error.message);
      this._retry.next({ count: 0, alarm });
    }
  }
}
