/**
 * 專案名稱： gof-proxy
 * 檔案說明： 報警服務匯出點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

export * from './alarm-retry.proxy';
export * from './alarm.service';
