/**
 * 專案名稱： gof-proxy
 * 檔案說明： 報警服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Produce } from '../../core';
import { Alarm } from '../models';
import { Producer } from '../producer';

/**
 * 報警服務
 */
export class AlarmService implements Produce {
  /**
   * @param _producer 資料生產者
   */
  constructor(private _producer = new Producer()) {}

  /**
   * 發送報警
   *
   * @method public
   * @param alarm 報警資料
   */
  public send(alarm: Alarm): void {
    this._producer.publish(alarm, (error, result) => {
      if (error) {
        throw new Error(error.message);
      } else {
        console.log('Send success');
      }
    });
  }
}
