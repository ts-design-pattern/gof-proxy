/**
 * 專案名稱： gof-proxy
 * 檔案說明： 資料生產者
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 資料生產者
 */
export class Producer {
  /**
   * 拋送次數
   */
  private _count = 1;

  /**
   * 拋送資料
   *
   * @method public
   * @param payload 要拋送的資料
   * @param cb      拋送結果回乎函數
   */
  public publish(
    payload: any,
    cb?: (error: Error | null, result: any) => void,
  ): void {
    if (cb) {
      if (this._count < 3) {
        this._count++;
        cb(new Error('Timeout'), null);
      } else {
        cb(null, payload);
      }
    }
  }
}
