/**
 * 專案名稱： gof-proxy
 * 檔案說明： 抽象發送行為
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 抽象發送行為
 */
export interface Produce {
  /**
   * 發送數據
   *
   * @method public
   * @param payload 要發送的數據
   */
  send(payload: any): void;
}
