/**
 * 專案名稱： gof-proxy
 * 檔案說明： 不好的範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { AlarmService } from './alarm.service';

const alarmService = new AlarmService();
alarmService.send({
  eventId: 'SYS001',
  syncId: 'WKS.F232.CA1',
  eventTime: 1609459200000,
  message: '停線超過 2 小時',
  alarmType: 1,
  level: 'L4',
});
