/**
 * 專案名稱： gof-proxy
 * 檔案說明： 報警服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as fs from 'fs';
import { Subject } from 'rxjs';
import { delay, filter, tap } from 'rxjs/operators';
import { Alarm } from './models';
import { Producer } from './producer/producer';

/**
 * 重拋資料格式
 */
type ErrorRetry = { count: number; alarm: Alarm };

/**
 * 報警服務
 */
export class AlarmService {
  /**
   * 重新拋送機制
   */
  private _retry = new Subject<ErrorRetry>();

  /**
   * @param _producer 資料生產者
   */
  constructor(private _producer = new Producer()) {
    this._retry
      .pipe(
        // 延遲 1 秒再送一次
        delay(1000),
        // 撰寫失敗日誌
        tap(res => this.writeLog(res)),
        // 次數上限 3 次
        filter(res => res.count <= 3),
      )
      .subscribe(res => this.retry(res));
  }

  /**
   * 撰寫失敗日誌
   *
   * @method private
   * @param response 補送結果
   */
  private writeLog(response: ErrorRetry): void {
    if (response.count > 3) {
      const filename = `./logs/${new Date().getTime()}.json`;
      if (!fs.existsSync('./logs')) {
        fs.mkdirSync('./logs');
      }
      fs.writeFileSync(filename, JSON.stringify(response.alarm));
    }
  }

  /**
   * 重新拋送
   *
   * @method private
   * @param response 補送結果
   */
  private retry(response: ErrorRetry): void {
    this._producer.publish(response.alarm, error => {
      console.log(`Retry ${response.count + 1} times`);
      if (error) {
        console.error(error.message);
        response.count++;
        this._retry.next(response);
      } else {
        console.log('Send success');
      }
    });
  }

  /**
   * 發送報警
   *
   * @method public
   * @param alarm 報警資料
   */
  public send(alarm: Alarm): void {
    this._producer.publish(alarm, error => {
      if (error) {
        console.error(error.message);
        this._retry.next({ count: 0, alarm });
      } else {
        console.log('Send success');
      }
    });
  }
}
