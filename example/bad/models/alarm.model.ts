/**
 * 專案名稱： gof-proxy
 * 檔案說明： 報警資料模型
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 報警資料模型
 */
export interface Alarm {
  /**
   * 事件 ID
   */
  eventId: string;
  /**
   * 報警 ID
   */
  syncId: string;
  /**
   * 報警時間
   */
  eventTime: number;
  /**
   * 報警信息
   */
  message: string;
  /**
   * 0: 表示報警解除；1: 表示報警持續中
   */
  alarmType: number;
  /**
   * 報警等級
   */
  level: string;
}
